### Steps

1. **Install Theme Package**
    * [Option 1: Upload Theme Package by Arena Installation App.](/summary/theme-installation/arena-installation-application.md)
    * [Option 2: Manual Upload Theme Package](/summary/theme-installation/manual-upload-theme.md)
2. [Install Requirement Shopfiy Apps](/app.md)
3. [Setup Currencies Option in your store](/summary/currency.md)
4. [Configure Shop](summary/theme-installation/stores.md)
5. [Setup Shopify Metafield for Extend Detail](/shopify-metafield.md)