### Configure Payment System

[Shopify Payment Official Documentation](https://help.shopify.com/en/manual/payments)


1. From your Shopify admin go to [Settings / Payments](http://shopify.com/admin/settings/payments)
2. Choose a payment gateway to accept payments for orders. The checkout page is working.
---
### Set your customer account preferences

[Shopify Account Customer Official Documentation](https://help.shopify.com/manual/customers/customer-accounts)


You can make customer accounts required or optional, or disable them altogether. When creating an account, customers are redirected to a separate account creation page. Keep in mind that requiring customers to create accounts might decrease sales conversions.

### Enable Customer Account

1. From your Shopify admin go to [Settings / Checkout](http://shopify.com/admin/settings/checkout)
2. In the block Customer accounts select Accounts are optional

![](/assets/enable_account.jpg)

---
### Configure Shipping Detail
[Shopify Official Documentation](https://help.shopify.com/en/manual/shipping)
[Shopify Shipping Settings Checklist](https://help.shopify.com/en/manual/shipping/shipping-checklist)

1. From your shop admin, go to [Settings / Shipping](http://shopify.com/admin/settings/shipping)
2. To enable shipping calculator, please configure your shipping rates by following below guide:

[Shopify Shipping Rate Configure](https://help.shopify.com/en/manual/shipping/rates-and-methods)

#### Show Shipping Calculate at Cart

1. From your Shopify admin, go to **Online Store > Themes**.
2. Find the theme that you want to edit and click **Customize**.
3. At **SHIPPING CALCULATOR** > Select **Show The Shipping Calculator** and insert **Default Country Selection**

![](/assets/shipping_calculator.png)