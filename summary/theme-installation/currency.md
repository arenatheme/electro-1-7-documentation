#Converting currencies in your store

You can use the multiple currencies script in your theme to show your prices in a currency that your customers are familiar with.

The multiple currencies script can be used to help your customers view product prices in a currency that they are familiar with, but the currency will always switch back to your store's currency at checkout. Your store's currency is the currency that you have set up on the [General settings](https://www.shopify.com/admin/settings/general) page of the admin.

## Show multiple currencies in a drop-down list


### Configure Shopify Money format

1. From your Shopify admin, go to **Settings**
2. Click **General**.
3. In the **Standards and formats** section, click the **Change formatting** link.
4. In the **HTML with currency field**, wrap the existing content in an HTML span element with a class name set to`money`.  
If your store currency is United States Dollars \(USD\), currency formatting is:
```
<span class=money>${{"{{amount}}"}} USD</span>
```
5. In the HTML without currency field, enter the exact same content that you entered in the HTML with currency field. Adding the currency descriptor to both formats will help to differentiate between currencies that use the dollar sign ($).  
If your store currency is United States Dollars (USD), currency formatting is:
```
<span class=money>{% raw  %}${{amount}}{% endraw  %}</span>
```
![Currency Format](/assets/currency.png)

More detail: [Shopify Currency Setting](https://help.shopify.com/en/manual/using-themes/change-the-layout/help-script-find-money-formats)

### Configure Currency Converter options
To configure your supported currencies:
1. From your Shopify admin go to Sales Channels > Online Store > Themes > Customize > **Theme settings** > **Cart Page**

2. Go to **MULTIPLE CURRENCY**  
![Currency Setting](/assets/currency_setting.png)

3. Check **Enable Multiple Currency** to display the option in your store.  
![Currency Converter](/assets/currency_converter.png)

4. Check Enable Currency Flags to show Flags with options  
![Currency Flags](/assets/currency_flags.png)

5. Check **Auto Currency Switcher** to use **IP Location service** matching Curency with the customer location. Eg. Currency auto change to GBP when customer come from UK

6. Under **Money Format**, choose whether to include the currency descriptor, such as USD or CAD, with the converted currency.

7. Under **Currencies You wish to Support**, enter the code for each currency you want to support. Separate the supported currency codes with a space.

8. Under **Default currency**, enter the code for the currency that you want each new customer to see. You'll probably want to use your store's operating currency.
[You can find a list of all of the currencies that are supported here.](http://www.xe.com/iso4217.php)  

9. Click **Save**.

At Mobile you may switch

![Currency Format](/assets/currency.gif)
