## Requirement Apps

By default, our theme support 2 Shopify apps, all is free.

**You must install to use following theme function**

### Install **Arena Wishlist Compare** App
Install URL: [ Arena Wishlist Compare](https://apps.arenatheme.com/install)

Wishlist & Compare is not offered by Shopify as a function so we have built a Free Shopify Application to assist customers.
- **Wishlist** is build by using Shopify App Proxy, it's not required to setup template page on your Shopify store. Wishlist function will launch at: https://your-shopify-url/apps/wishlist
- Compare required to** create and assign a separate page for the Compare page display**

#### Steps
1. From your Shopify admin, go to **Online Store &gt; Themes**. Click **Action** and **Public** **Maison** become **Current theme**.
_Eg. I would like to install Wishlist & Compare for **Maison** theme
_![](/assets/currenttheme.png)

2. Go to [https://apps.arenatheme.com/install](https://apps.arenatheme.com/install)
3. Input your Shopify Store url to the form and click **Install** 
![](/assets/wishlist-install.png)
4. Sign in your store and accept permission to Install app.
5. From your Shopify admin, go to **Apps** > **Arena Wishlist Compare **. Please ensure that you Wishlist & Compare app setting at **Enable** status
![](/assets/status wc.png)
6. From your Shopify admin, Go to **Online Store &gt; Pages **Click **Add Page **to creat page:  **Compare**. Click **Save**.
![](/assets/compare.png).
7. From your Shopify admin, go to **Online Store &gt; Themes > Customize > Theme Settings > Cart Page**. Click and select **Enable Wishlist & Enable Compare.** 
Let see how it work on your theme

> You may Reinstall App when you change current theme to another theme by click **Install**. It's will start to generate compare file for your theme.
> You can also change option for Compare function:
> ![](/assets/compareoption.png)

If you would like to add more function, please [contact us](https://arenathemes.freshdesk.com/support/tickets/new).

### Install Product Review.
#### Steps
1. Go to the App Store, and choose the app [Shopify Product Reviews](https://apps.shopify.com/product-reviews)
2. Click the **Get button**.
3. Log into the App Store.
4. Confirm App installation.
5. Use the app!

When finish install review app, You may setting to diplay Review on your Online Store by Setting at Product Sections metion [here](/products.md)

### Compatible Shopify App.

* Orbelo [https://apps.shopify.com/oberlo](https://apps.shopify.com/oberlo)