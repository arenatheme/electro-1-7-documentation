### Configure Header Section

1. From your Shopify admin, go to **Online Store &gt; Themes**
2. Next to the Theme, click **Customize**
3. From the top bar drop-down menu, Select **Home Page**
4. Select **Header** Section to configure
5. Click Save.

### Header Configuration Options
Parameter | Description
- |:-
Section Layout Mode| Boxed: fixed layouts at Box Container <br>Wide: show entire screen 
Header Style | ![header style](/assets/header_style.png)
Border bottom for header | Show a line at header bottom
**Sticky Navigation** | Sticky Your Navigation when scroll down on your online store
Logo image | Upload your logo Image. <br>Recomended image size width = 2 * Custom logo width.
Custom logo width | Setting Your logo width to auto crop the image to this size
TOP ANNOUNCEMENT BAR | ![Header top bar](/assets/header_topbar.png)<br> You may configure Topbar Color at [Theme Setting](settings/styles.md#colors)
MENU (NAVIGATION) | Configure Navigation List on your Online Store <br>Main menu: Default Menu<br>Left menu: Left Side Navigation Link list on Header Style 2, 3<br>Right menu: Left Side Navigation Link list on Header Style 2, 3<br>HAMBURGER MENU: Menu link list show when click Hamburger icon<br>It's require to [add a menu items](https://help.shopify.com/en/manual/sell-online/online-store/menus-and-links/editing-menus#add-a-menu-item) to select in the option 
MEGA NAVIGATION & MENU FLYOUT | Enable Mega Navigation: Enable Mega Navigation Option <br>
NAVIGATION LABEL | Show Navigation Label catch by **menu handles**
CONTENT | Mega Menu Navigation Block Content. You can use block content to creats up to 25 Mega Menu (Shopify limit 25 blocks Section at Homepage)

### Navigation handles

Handles are unique identifiers within Shopify for products, collections, link lists, and pages. A handle is used to reference a menu in Liquid. e.g. a menu with the title **“Sidebar menu”** would have the handle **sidebar-menu** by default



