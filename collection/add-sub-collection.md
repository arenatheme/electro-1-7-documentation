![](/assets/sub_collection_list.png)

1. Create nestest menu to control hierarchy
2. Assign nestest menu to Sub-Collection content.


### Creat Nested Menu to  your collections hierarchy. 

More detail about Nestest menu: https://help.shopify.com/en/themes/development/building-nested-navigation

* **Creat a collection nested menu**. You can see that Decor Art have 3 Sub collection: Personal, Romatic, Special Goods.

![](/assets/menu-items.png)


### Assign Nested Menu to display your Sub-collecton. 

* From shopify admins, go to **Online Store** > **Themes**
* Find the theme that you want to edit and click **Customize**
* From the top bar drop-down menu, select the type of page that you want to edit. **Select** **Collection pages**
* Go to **CONTENT** > **Add Content** > **Sub-Collection**
* Select **Menu items**
* Click **Save**
