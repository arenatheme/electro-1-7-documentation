## About us Page

[<button class = "markdown-button" name="button">LINK TO DEMO PAGE</button>](https://arena-Maison.myshopify.com/pages/about-us)

1. [Create new page](../pages.md#creat-a-page)
2. Select template "page.about"
3. Save page
4. [Customize Page Content by Section](../pages.md#customize-page-by-sections)

### Customize Page Content

Ensure you have an article with page.about template (or blog post) published.

1. From your Shopify admin, to the **Online Store > Themes **page and use the Customize theme button to start customizing your theme.
2. From the top bar drop-down menu, select **About us** page to configure.
![About us select](/assets/about_us_select.png)
3. Open the Page settings.

About us support 5 blocks content type:

* Testimonial
* Logo List
* Team member (Our team)
* Banner & Text
* Document: Block show special text content