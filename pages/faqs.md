### Creat FAQs Page

[<button class = "markdown-button" name="button">LINK TO DEMO PAGE</button>](https://arena-Maison.myshopify.com/pages/faq)

1. [Create new page](../pages.md#creat-a-page)
2. Select template "page.faq"
3. Save page
4. [Customize Page Content by Section](../pages.md#customize-page-by-sections)

### Add FAQs Content

Ensure you have an article with page.faq template (or blog post) published.

1. From your Shopify admin, go to **Online Store &gt; Themes** and use the Customize theme button to start customizing your theme.

2. From the top bar drop-down menu, Select **FAQ** Page to configure

3. Open the Page settings by Click **FAQ page** at **Sections Sidebar**. It's show setting & content for your FAQs.
4. Click to **FAQ Page** Section at sidebar to customize content.
You may customize layout and add content depend on template.

5. Click **Save** when you finish customize