### General

Parameter | Description
- |:-:
Enable Lazy Loading| Enable Lazy loadding with image loading to optimize your online store speed.
Button Back To Top| Enable button Back to Top on Desktop
Button Search Function| Enable Auto Suggestion Search
Enable Wishlist & Compare App| Show Wishlist Compare icon on your Online Store
Quick View Product | Enable Quickview Option
Disable Ajax Cart | Customer have to click update price at cart page to update their cart
Enable Catalog Mode | Turn off Add to cart button.

### Google Translate

Parameter | Description
- |:-:
Show Google Translate | Show Google translate select button in your store
Language you wish to translate | Insert language code to display at selection

### Product Default Template

Select default Product Template to Product. More detail about [Page alternate template](/pages.md#page-alternate-templates)

### Collection Page

Configure settings at Collection Pages (use for all collection alternate templates)  

Parameter | Description
- |:-:
Mode view default| Select collection default template for Product Grid / List
Enable Slideshow | Enable Slideshow Static Section in Collection Page
Number of Product per row| How many products show per row at Product Grid / List
Enable product's title blance | Auto balance product title long

### PORTFOLIO PAGE

Enable slideshow | Enable Slideshow Static Section in Profolio page

### EU Cookie Policy

| Parameter | Description|
| - |:-:|
| EU Cookie Policy| Enable EU Cookie Policy  |
| EU Cookie AutoDetect| Auto Detect Visitor from EU Country to show EU Cookie Policy by GeoIP Service|
| Position | Position to show EU Cookie Popup on Desktop <br>Bottom/Top/Floating left/Floating right|
| Link to own policy | Customer will go out and vistit the link if they dont alow to use Cookie |
| EU Cookie Color Options | Configure color for EU Cookie Popup |