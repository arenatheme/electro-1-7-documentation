### Mailchimp Newsletter Popup
Parameter | Description
- |:-
Popup Newsletter Style | Delay Time: Set time delay to show Popup<br>Exit Intent Popup: An exit popup is a message that displays to users as they are attempting to navigate away from your site.<br>None: disable Newsletter Popup
Popup image | Image size should be 585x600 pixels for best view
Popup Text Color | Text color at Popup
Heading | Add heading
Caption | Add Caption
Mailing Form Action | Add Mailchimp Mailchimp signup URL
Delay time (milliseconds) | Number of seconds to display Popup with Delay Time option.<br>1000 milisecond = 1 second
Placeholder | Placeholder text
Expires (day) | Popup wont display on next x day