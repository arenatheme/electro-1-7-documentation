### Product Grid Setting

Parameter | Description
- |:-
Layout style | ![Product Grid Style](/assets/product_grid_style.png)
Text align for product item | ![Product Text Align](/assets/text_align.png)
Hover effect| None: Dont show hover effect <br>Overlay: Add Overlay color above image<br>Switch image: display Second product image when hover to product.
Overlay color | configure color to overlay
Sticky add to cart Mobile | Show Add to cart on Mobile <br> ![Mobile Sticky Add to cart](/assets/mobile_sticky_cart.png)
Show countdown timer | ![Product countdownt at product grid](/assets/productgrid_countdown.png)
Swatch item | Configure Swatch icon show at Product Grid <br>Show swatch color: add swatch color options to product grid<br>Show swatch size: add swatch size options to Product grid<br>Use variant images for swatch color: use product image instead of Swatch Color<br>Limit swatch items showing: Limit swatch items to show
Product Labels | Enabel new label: Show new label<br>Enabel hot label: Show hot label<br>Enabel sale label: show sale label
