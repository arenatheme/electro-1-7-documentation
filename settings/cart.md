### BLOCK SHOPPING CART
Parameter | Description
 - |:-
Cart Type on Desktop | Select your cart type to display <br><ul><li>Dropdown: dropdown modal</li> <li>Cart Drawer: Off canvas menu modal</li></ul>

### FREE SHIPPING THRESHOLD

In simple terms: If the threshold is set at $100 and your average order value is $50, customers won't be willing to double up their order to get the offer. Your better option is to lower the threshold so that it's attainable for the average customer.

Parameter | Description
 - |:-
Enable free shipping threshold | Select to enable
Price to free shipping | Set price to match free shipping condition (in USD)

### MULTIPLE CURRENCY

Parameter | Description
 - |:-
Enable Multiple Currency | Select to enable
Enable Currency Switcher by Geo IP Location | Enable Geo ip location service to auto switch currency match with vistor location
Enable Currency Symbols On Mobile | Using Currency Symbol to display as select Currency icon in Mobile device

### ADD TO CART REDIRECT

Parameter | Description
 - |:-
Enable redirect to checkout after add to cart | Enable if you would like to redirect customer to checkout page right after add to cart