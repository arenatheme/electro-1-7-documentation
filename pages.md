If you have a Shopify online store, then you can create webpages in your Shopify admin. Webpages contain information that rarely changes or that customers will reference often, like an "About Us" page or a "Contact Us" page.

You can also [add a blog](https://help.shopify.com/en/manual/sell-online/online-store/blogs) to your online store for content that you'll be updating regularly, or if you want to encourage feedback.

Before start to add new webpage, please make sure that you already **Publish** Maison theme by click **Action** & **Publish**.

![publish](/assets/publish.png)

![setpublish](/assets/setpublish.png)

### Create a Page

From your Shopify admin, go to **Online Store**&gt;**Pages**.

1. Click **Add page**. You will be taken to a new webpage editor.

2. In the webpage editor, enter a **Title **and **Content **in the text boxes provided.

   Be descriptive and clear when choosing your webpage title. The webpage title is displayed in the tab or title bar of browsers. It is also used as the title in search engine results.[Learn more about website SEO.](https://help.shopify.com/en/manual/promoting-marketing/seo)

3. In the **Visibility **field, you can select whether you want your webpage to be published or not. By default, your new webpage will be visible when you click **Save**. Select the **Hidden **option if you want your new webpage to be hidden from your online store, or click **Set a specific publish date **to [control when your webpage is published](https://help.shopify.com/en/manual/productivity-tools/future-publishing):

4. In the **Template** field, you can select page template \(page type\) that you would like to apply.

![setpublish](/assets/page_template.png)

5. Click **Save**

### Page alternate templates
[<button class = "markdown-button" name="button"> ALTERNATE TEMPLATE SHOPIFY OFFICIAL DOCUMENT</button>](https://help.shopify.com/en/themes/customization/store/create-alternate-templates)

Maison support 5 page altenate templates \(page type\):

| Page Template         |Template name      | Description |  Quantinty Page Limit |
| - |:-:| -:-|-:-|
| Page default  | page | Default page | infinity |
| [Page About](pages/about-us.md)    | page.about |  Template with page About section | 1 |
| [Page Contact](pages/contact.md) | page.contact |  Template with Contact section | 1 |
| [Page FAQ](pages/faqs.md) | page.faq| Template with Frequently asked questions (FAQ) | 1 |
| [Page Store Locator](pages/store-location.md)| page.store,locator | Template support add unlimited Store location| 1 | 
| Page Compare | page.bc_compare| Template created by **Arena Wishlist Compare** App with Compare function | 1 |

### Customize Page by Sections

In order to customize layout for pages

From your Shopify

1. Find the theme that you want to edit and click **Customize**.

2. **From the top bar drop-down menu, select the page that you want to configure.**

3. **Change your Section setting** and click **Save**


