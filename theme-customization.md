## Before you customize your theme

Before you customize your theme, it's a good idea to do the following tasks:

* [Duplicate your theme](https://help.shopify.com/en/manual/using-themes/managing-themes/duplicating-themes) to create a backup copy. This makes it easy to discard your changes and start again if you need to.

## Customizing themes

You can customize your theme settings from your Shopify admin by using the theme editor. The theme editor includes a theme preview and a toolbar that you can use to add and remove content, and to make changes to your settings.

The theme editor toolbar is divided into **Sections** and **Theme** settings.

![Theme Editor](https://help.shopify.com/assets/themes/editor-tabs-d6a578d45bf5feac7da3f618de523b00eacf724049c10e6cd6348fd061bb7e1f.png)

* **Theme settings** like backbone of a theme included settings default for your Shopify store at all pages.
.You can edit your **theme settings** to customize your online **store's content, layout, typography, and colors**. Each theme provides settings that allow you to change the look and feel of your store without editing any code.

* You can use **Sections** to modify the content and layout of the different pages on your store. You can use theme settings to customize your store's fonts and colors, and make changes to your social media links and checkout settings.

Different types of pages have different **theme sections**. When you first open the **theme editor**, the settings for the **home page** are shown. To access the settings for other pages, select the page type from the top bar drop-down menu:

![](https://help.shopify.com/assets/manual/customize/editor-product-e42f3cd79a23002e746a785271cc687537498560926798c5ccbff819050a2bf0.jpg)

You can use sections to modify the content and layout of the different pages on your store. You can use theme settings to customize your store's fonts and colors, and make changes to your social media links and checkout settings.

### Go to Theme Editor

* From your Shopify admin, go to **Online Store** &gt; **Themes**.
* Find the theme that you want to edit and click **Customize**.
