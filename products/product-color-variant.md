## Select Product Variant Option

You can use color images or product variants image to show as product detail option

![](/assets/product_color.png)

### Configure Theme Setting to Enable swatch color

1. From your Shopify admin, go to **Online Store &gt; Themes.**
2. Find the theme that you want to edit and click **Customize**.
3. From the top bar drop-down menu, select **Product pages.** Now you will access to edit Sections for the Product page.
4. In the **preview of your store on the right side of the page**, navigate to your product **that uses the product.alternate** page template.
5. Open the Product (alternate) pages settings.
6. Go to COLOR AND SIZE SWATCHES. 
7. Click check **Adding Color Swatches**.
![](/assets/product_color_swatch.png)
8, Click **Save**

### Upload your color images to Theme asset

![](/assets/color_image.png)

For the color variant options you have, you can either let the smartness of the script provide a color for you, or you can upload an image that represents that color.

_ The way I have gotten my images from my products in my demo shop was to open up each product image on my storefront in the “colorbox” \(lightbox\) and grab around ~ 40 by 40 pixels section of the product image, then save that small screen grab to my desktop, and rename the image.  _

> There's an important naming convention to respect here! The image must be named after the color option, but be handleized, and have a .png extension.

For example, if you have a color called 'Déjà Vu Blue', then name your image deja-vu-blue.png
##### Steps

Other example, if your color is 'Blue/Gray', then name your image blue-gray.png.

1. Most simple example, if your color is 'Red', the name your image red.png.
2. From your Shopify admin, go to **Online Store &gt; Themes.**
3. Find the theme you want to edit, and then click **Actions &gt; Edit code.**
4. On the **Edit HTML/CSS page**, locate and click on the **Assets** folder to reveal its content.
5. Under the **Assets** heading, click on the **Add** a new asset link.
6. **Upload your image.**

Repeat steps 5 and 6 until you have uploaded all your images.