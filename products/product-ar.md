Shopify AR allows customers using the Safari browser on iOS 12 devices to view realistic and interactive versions of your products in augmented reality (AR). With AR you can provide your customers with a better sense of the size, scale, and detail of your products. The 3D Warehouse app enables AR experiences by allowing you to upload 3D models and link them to products on your store.

To allow customers to view your products in AR, [install the 3D Warehouse app](https://help.shopify.com/en/manual/apps/apps-by-shopify/3d-warehouse#step-1-install-the-3d-warehouse-app), get 3D models of your products, add the 3D models to the app, enable AR Quick Look in the theme.

### Step 1: Install the 3D Warehouse app

You can install the 3D Warehouse app using the sign-up page.

#### Steps:
1. Open the [sign-up page](https://threed-warehouse.shopifycloud.com/).
2. Enter your myshopify.com store domain.
3. Click Install.
4. Review the details, then click Install app.

You can access the 3D Warehouse app on the [Apps page](https://www.shopify.com/admin/apps) of your Shopify admin.

### Step 2: Get 3D models of your products

You may hire expert to create 3D model for your products. 

### Step 3: Add a 3D model to the 3D Warehouse app

When you have the 3D Warehouse app installed and have sourced a 3D model, you need to add the model to the 3D Warehouse app.

####Steps:
1. From your Shopify admin, click [**Apps**](https://www.shopify.com/admin/apps).
2. Click **3D Warehouse**.
3. Click Add 3D Model.
4. In the Title field, enter a title for the 3D model.
5. In the Linked Product field, select the product and variant that this 3D model is associated with, then click Select product.
6. Click **Upload File** and select the .usdz file provided by the Shopify Expert.
7. Click **Save**.

Repeat the upload process for the .gltf or .glb file that your Shopify Expert provided. Make sure you link it to the same product so that you have both 3D model file types associated with the product.

> Note
> If you upload a .gltf file, you're prompted to add additional files in the Linked Files section. Click Upload File to upload the linked texture resources provided by your expert.


**Accepted file types **
> Shopify Experts provide two different file types for each 3D model, a .usdz file and a .gltf or .glb file. These different file types are used by different platforms and features. For example, for customers to view 3D products in the Safari browser on iOS 12 devices, you need to upload a .usdz file. The best way to make sure that your online store is compatible with future augmented reality and 3D features is to upload both file types.

### Step 4: Configure the theme to Enable 3D Warehouse.

1. From your Shopify admin, go to **Online Store &gt; Themes**
2. Find the theme click **Customize**
3. From the top bar drop-down menu, select **Product pages.** Now you will access to edit Sections for the Product page.
4. In the **preview of your store on the right side of the page**, navigate to your product **that uses the product.alternate** page template.
5. Open the Product (alternate) pages settings.
6. Tick **Enable 3D Warehouse**
> It's only support few product alternate template. Please check it [here](/products.md)
![3D warehouse enable](/assets/3d_warehouse.png)
7. Click **Save**
